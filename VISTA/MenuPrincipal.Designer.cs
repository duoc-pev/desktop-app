﻿namespace VISTA
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuPrincipal));
            this.mpnl_principal = new MetroFramework.Controls.MetroPanel();
            this.btn_usuarios = new MetroFramework.Controls.MetroButton();
            this.btn_boletas = new MetroFramework.Controls.MetroButton();
            this.btn_clientes = new MetroFramework.Controls.MetroButton();
            this.btn_salirDelSistema = new MetroFramework.Controls.MetroButton();
            this.btn_venta = new MetroFramework.Controls.MetroButton();
            this.htmlToolTip1 = new MetroFramework.Drawing.Html.HtmlToolTip();
            this.mpnl_principal.SuspendLayout();
            this.SuspendLayout();
            // 
            // mpnl_principal
            // 
            this.mpnl_principal.AutoSize = true;
            this.mpnl_principal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mpnl_principal.BackgroundImage")));
            this.mpnl_principal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mpnl_principal.Controls.Add(this.btn_venta);
            this.mpnl_principal.Controls.Add(this.btn_salirDelSistema);
            this.mpnl_principal.Controls.Add(this.btn_clientes);
            this.mpnl_principal.Controls.Add(this.btn_boletas);
            this.mpnl_principal.Controls.Add(this.btn_usuarios);
            this.mpnl_principal.HorizontalScrollbarBarColor = true;
            this.mpnl_principal.HorizontalScrollbarHighlightOnWheel = false;
            this.mpnl_principal.HorizontalScrollbarSize = 10;
            this.mpnl_principal.Location = new System.Drawing.Point(24, 64);
            this.mpnl_principal.Name = "mpnl_principal";
            this.mpnl_principal.Size = new System.Drawing.Size(1339, 593);
            this.mpnl_principal.TabIndex = 0;
            this.mpnl_principal.UseCustomForeColor = true;
            this.mpnl_principal.UseStyleColors = true;
            this.mpnl_principal.VerticalScrollbarBarColor = true;
            this.mpnl_principal.VerticalScrollbarHighlightOnWheel = false;
            this.mpnl_principal.VerticalScrollbarSize = 10;
            // 
            // btn_usuarios
            // 
            this.btn_usuarios.BackColor = System.Drawing.Color.Gray;
            this.btn_usuarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_usuarios.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btn_usuarios.Location = new System.Drawing.Point(779, 276);
            this.btn_usuarios.Name = "btn_usuarios";
            this.btn_usuarios.Size = new System.Drawing.Size(113, 185);
            this.btn_usuarios.TabIndex = 2;
            this.btn_usuarios.Text = "USUARIOS";
            this.btn_usuarios.UseCustomBackColor = true;
            this.btn_usuarios.UseCustomForeColor = true;
            this.btn_usuarios.UseSelectable = true;
            this.btn_usuarios.UseStyleColors = true;
            // 
            // btn_boletas
            // 
            this.btn_boletas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_boletas.DisplayFocus = true;
            this.btn_boletas.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btn_boletas.Location = new System.Drawing.Point(548, 158);
            this.btn_boletas.Name = "btn_boletas";
            this.btn_boletas.Size = new System.Drawing.Size(344, 96);
            this.btn_boletas.TabIndex = 3;
            this.btn_boletas.Text = "BOLETAS";
            this.btn_boletas.UseCustomBackColor = true;
            this.btn_boletas.UseCustomForeColor = true;
            this.btn_boletas.UseSelectable = true;
            this.btn_boletas.UseStyleColors = true;
            this.btn_boletas.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // btn_clientes
            // 
            this.btn_clientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_clientes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_clientes.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btn_clientes.Location = new System.Drawing.Point(548, 276);
            this.btn_clientes.Name = "btn_clientes";
            this.btn_clientes.Size = new System.Drawing.Size(204, 185);
            this.btn_clientes.TabIndex = 5;
            this.btn_clientes.Text = "CLIENTES";
            this.btn_clientes.UseCustomBackColor = true;
            this.btn_clientes.UseCustomForeColor = true;
            this.btn_clientes.UseSelectable = true;
            this.btn_clientes.UseStyleColors = true;
            // 
            // btn_salirDelSistema
            // 
            this.btn_salirDelSistema.BackColor = System.Drawing.Color.Transparent;
            this.btn_salirDelSistema.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_salirDelSistema.BackgroundImage")));
            this.btn_salirDelSistema.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_salirDelSistema.ForeColor = System.Drawing.Color.Transparent;
            this.btn_salirDelSistema.Location = new System.Drawing.Point(1241, 513);
            this.btn_salirDelSistema.Name = "btn_salirDelSistema";
            this.btn_salirDelSistema.Size = new System.Drawing.Size(68, 54);
            this.btn_salirDelSistema.TabIndex = 6;
            this.btn_salirDelSistema.Text = "Salir";
            this.btn_salirDelSistema.UseCustomBackColor = true;
            this.btn_salirDelSistema.UseCustomForeColor = true;
            this.btn_salirDelSistema.UseSelectable = true;
            this.btn_salirDelSistema.UseStyleColors = true;
            this.btn_salirDelSistema.Click += new System.EventHandler(this.metroButton5_Click);
            // 
            // btn_venta
            // 
            this.btn_venta.BackColor = System.Drawing.Color.Blue;
            this.btn_venta.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btn_venta.Location = new System.Drawing.Point(361, 158);
            this.btn_venta.Name = "btn_venta";
            this.btn_venta.Size = new System.Drawing.Size(165, 303);
            this.btn_venta.TabIndex = 7;
            this.btn_venta.Text = "VENTA";
            this.htmlToolTip1.SetToolTip(this.btn_venta, "Menu ventas\r\n\r\n* Venta de productos\r\n* Venta fiado");
            this.btn_venta.UseCustomBackColor = true;
            this.btn_venta.UseCustomForeColor = true;
            this.btn_venta.UseSelectable = true;
            this.btn_venta.UseStyleColors = true;
            // 
            // htmlToolTip1
            // 
            this.htmlToolTip1.OwnerDraw = true;
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1386, 708);
            this.ControlBox = false;
            this.Controls.Add(this.mpnl_principal);
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MenuPrincipal";
            this.Text = "Menu Principal";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.mpnl_principal.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroPanel mpnl_principal;
        private MetroFramework.Controls.MetroButton btn_salirDelSistema;
        private MetroFramework.Controls.MetroButton btn_clientes;
        private MetroFramework.Controls.MetroButton btn_boletas;
        private MetroFramework.Controls.MetroButton btn_usuarios;
        private MetroFramework.Controls.MetroButton btn_venta;
        private MetroFramework.Drawing.Html.HtmlToolTip htmlToolTip1;
    }
}
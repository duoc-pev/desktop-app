﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace VISTA
{
    public partial class Login : MetroForm
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btn_borrar_txt_usuario_Click(object sender, EventArgs e)
        {
            txt_usuario.Clear();
        }

        private void btn_borrar_txt_pass_Click(object sender, EventArgs e)
        {
            txt_password.Clear();
        }

        private void lbl_recup_contrasena_Click(object sender, EventArgs e)
        {
            pnl_recupContrasena rc = new pnl_recupContrasena();
            rc.Show();
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            
            MenuPrincipal mp = new MenuPrincipal();
            mp.Show();
            this.Hide();
            
        }
    }
}

﻿namespace VISTA
{
    partial class Login
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.btn_borrar_txt_usuario = new MetroFramework.Controls.MetroButton();
            this.btn_borrar_txt_pass = new MetroFramework.Controls.MetroButton();
            this.lbl_recup_contrasena = new MetroFramework.Controls.MetroLabel();
            this.lbl_password = new MetroFramework.Controls.MetroLabel();
            this.btn_login = new MetroFramework.Controls.MetroButton();
            this.txt_usuario = new MetroFramework.Controls.MetroTextBox();
            this.txt_password = new MetroFramework.Controls.MetroTextBox();
            this.lbl_usuario = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.Black;
            this.metroPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroPanel1.BackgroundImage")));
            this.metroPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.metroPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel1.Controls.Add(this.btn_borrar_txt_usuario);
            this.metroPanel1.Controls.Add(this.btn_borrar_txt_pass);
            this.metroPanel1.Controls.Add(this.lbl_recup_contrasena);
            this.metroPanel1.Controls.Add(this.lbl_password);
            this.metroPanel1.Controls.Add(this.btn_login);
            this.metroPanel1.Controls.Add(this.txt_usuario);
            this.metroPanel1.Controls.Add(this.txt_password);
            this.metroPanel1.Controls.Add(this.lbl_usuario);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 63);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(440, 350);
            this.metroPanel1.TabIndex = 8;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // btn_borrar_txt_usuario
            // 
            this.btn_borrar_txt_usuario.BackColor = System.Drawing.Color.Transparent;
            this.btn_borrar_txt_usuario.ForeColor = System.Drawing.Color.White;
            this.btn_borrar_txt_usuario.Location = new System.Drawing.Point(365, 76);
            this.btn_borrar_txt_usuario.Name = "btn_borrar_txt_usuario";
            this.btn_borrar_txt_usuario.Size = new System.Drawing.Size(26, 23);
            this.btn_borrar_txt_usuario.TabIndex = 5;
            this.btn_borrar_txt_usuario.Text = "X";
            this.btn_borrar_txt_usuario.UseCustomBackColor = true;
            this.btn_borrar_txt_usuario.UseCustomForeColor = true;
            this.btn_borrar_txt_usuario.UseSelectable = true;
            this.btn_borrar_txt_usuario.UseStyleColors = true;
            this.btn_borrar_txt_usuario.Click += new System.EventHandler(this.btn_borrar_txt_usuario_Click);
            // 
            // btn_borrar_txt_pass
            // 
            this.btn_borrar_txt_pass.BackColor = System.Drawing.Color.Transparent;
            this.btn_borrar_txt_pass.ForeColor = System.Drawing.Color.White;
            this.btn_borrar_txt_pass.Location = new System.Drawing.Point(365, 156);
            this.btn_borrar_txt_pass.Name = "btn_borrar_txt_pass";
            this.btn_borrar_txt_pass.Size = new System.Drawing.Size(26, 23);
            this.btn_borrar_txt_pass.Style = MetroFramework.MetroColorStyle.White;
            this.btn_borrar_txt_pass.TabIndex = 6;
            this.btn_borrar_txt_pass.Text = "X";
            this.btn_borrar_txt_pass.Theme = MetroFramework.MetroThemeStyle.Light;
            this.btn_borrar_txt_pass.UseCustomBackColor = true;
            this.btn_borrar_txt_pass.UseCustomForeColor = true;
            this.btn_borrar_txt_pass.UseSelectable = true;
            this.btn_borrar_txt_pass.UseStyleColors = true;
            this.btn_borrar_txt_pass.Click += new System.EventHandler(this.btn_borrar_txt_pass_Click);
            // 
            // lbl_recup_contrasena
            // 
            this.lbl_recup_contrasena.AutoSize = true;
            this.lbl_recup_contrasena.BackColor = System.Drawing.Color.Transparent;
            this.lbl_recup_contrasena.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lbl_recup_contrasena.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lbl_recup_contrasena.ForeColor = System.Drawing.Color.White;
            this.lbl_recup_contrasena.Location = new System.Drawing.Point(157, 306);
            this.lbl_recup_contrasena.Name = "lbl_recup_contrasena";
            this.lbl_recup_contrasena.Size = new System.Drawing.Size(130, 15);
            this.lbl_recup_contrasena.TabIndex = 7;
            this.lbl_recup_contrasena.Text = "Recuperar Contraseña";
            this.lbl_recup_contrasena.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lbl_recup_contrasena.UseCustomBackColor = true;
            this.lbl_recup_contrasena.UseCustomForeColor = true;
            this.lbl_recup_contrasena.UseStyleColors = true;
            this.lbl_recup_contrasena.Click += new System.EventHandler(this.lbl_recup_contrasena_Click);
            // 
            // lbl_password
            // 
            this.lbl_password.AutoSize = true;
            this.lbl_password.BackColor = System.Drawing.Color.Transparent;
            this.lbl_password.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lbl_password.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lbl_password.ForeColor = System.Drawing.Color.White;
            this.lbl_password.Location = new System.Drawing.Point(59, 164);
            this.lbl_password.Name = "lbl_password";
            this.lbl_password.Size = new System.Drawing.Size(73, 15);
            this.lbl_password.TabIndex = 4;
            this.lbl_password.Text = "PASSWORD";
            this.lbl_password.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lbl_password.UseCustomBackColor = true;
            this.lbl_password.UseCustomForeColor = true;
            this.lbl_password.UseStyleColors = true;
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.Color.MidnightBlue;
            this.btn_login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_login.ForeColor = System.Drawing.Color.White;
            this.btn_login.Location = new System.Drawing.Point(59, 230);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(332, 41);
            this.btn_login.TabIndex = 0;
            this.btn_login.Text = "INGRESAR";
            this.btn_login.Theme = MetroFramework.MetroThemeStyle.Light;
            this.btn_login.UseCustomBackColor = true;
            this.btn_login.UseCustomForeColor = true;
            this.btn_login.UseSelectable = true;
            this.btn_login.UseStyleColors = true;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // txt_usuario
            // 
            this.txt_usuario.AccessibleDescription = "Ingrese su nombre de usuario";
            this.txt_usuario.BackColor = System.Drawing.Color.LightGray;
            // 
            // 
            // 
            this.txt_usuario.CustomButton.Image = null;
            this.txt_usuario.CustomButton.Location = new System.Drawing.Point(201, 1);
            this.txt_usuario.CustomButton.Name = "";
            this.txt_usuario.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txt_usuario.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_usuario.CustomButton.TabIndex = 1;
            this.txt_usuario.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_usuario.CustomButton.UseSelectable = true;
            this.txt_usuario.CustomButton.Visible = false;
            this.txt_usuario.ForeColor = System.Drawing.Color.Black;
            this.txt_usuario.Lines = new string[0];
            this.txt_usuario.Location = new System.Drawing.Point(144, 76);
            this.txt_usuario.MaxLength = 32767;
            this.txt_usuario.Name = "txt_usuario";
            this.txt_usuario.PasswordChar = '\0';
            this.txt_usuario.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_usuario.SelectedText = "";
            this.txt_usuario.SelectionLength = 0;
            this.txt_usuario.SelectionStart = 0;
            this.txt_usuario.ShortcutsEnabled = true;
            this.txt_usuario.Size = new System.Drawing.Size(223, 23);
            this.txt_usuario.TabIndex = 1;
            this.txt_usuario.UseCustomBackColor = true;
            this.txt_usuario.UseCustomForeColor = true;
            this.txt_usuario.UseSelectable = true;
            this.txt_usuario.UseStyleColors = true;
            this.txt_usuario.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_usuario.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txt_password
            // 
            this.txt_password.BackColor = System.Drawing.Color.LightGray;
            // 
            // 
            // 
            this.txt_password.CustomButton.Image = null;
            this.txt_password.CustomButton.Location = new System.Drawing.Point(201, 1);
            this.txt_password.CustomButton.Name = "";
            this.txt_password.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txt_password.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_password.CustomButton.TabIndex = 1;
            this.txt_password.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_password.CustomButton.UseSelectable = true;
            this.txt_password.CustomButton.Visible = false;
            this.txt_password.Lines = new string[0];
            this.txt_password.Location = new System.Drawing.Point(144, 156);
            this.txt_password.MaxLength = 32767;
            this.txt_password.Name = "txt_password";
            this.txt_password.PasswordChar = '*';
            this.txt_password.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_password.SelectedText = "";
            this.txt_password.SelectionLength = 0;
            this.txt_password.SelectionStart = 0;
            this.txt_password.ShortcutsEnabled = true;
            this.txt_password.Size = new System.Drawing.Size(223, 23);
            this.txt_password.TabIndex = 2;
            this.txt_password.UseCustomBackColor = true;
            this.txt_password.UseCustomForeColor = true;
            this.txt_password.UseSelectable = true;
            this.txt_password.UseStyleColors = true;
            this.txt_password.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_password.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lbl_usuario
            // 
            this.lbl_usuario.AutoSize = true;
            this.lbl_usuario.BackColor = System.Drawing.Color.Transparent;
            this.lbl_usuario.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lbl_usuario.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lbl_usuario.ForeColor = System.Drawing.Color.White;
            this.lbl_usuario.Location = new System.Drawing.Point(59, 84);
            this.lbl_usuario.Name = "lbl_usuario";
            this.lbl_usuario.Size = new System.Drawing.Size(61, 15);
            this.lbl_usuario.TabIndex = 3;
            this.lbl_usuario.Text = "USUARIO";
            this.lbl_usuario.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lbl_usuario.UseCustomBackColor = true;
            this.lbl_usuario.UseCustomForeColor = true;
            this.lbl_usuario.UseStyleColors = true;
            // 
            // Login
            // 
            this.AccessibleDescription = "";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 443);
            this.Controls.Add(this.metroPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Login";
            this.Text = "Acceso Usuarios";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroButton btn_login;
        private MetroFramework.Controls.MetroTextBox txt_usuario;
        private MetroFramework.Controls.MetroTextBox txt_password;
        private MetroFramework.Controls.MetroLabel lbl_usuario;
        private MetroFramework.Controls.MetroLabel lbl_password;
        private MetroFramework.Controls.MetroButton btn_borrar_txt_usuario;
        private MetroFramework.Controls.MetroButton btn_borrar_txt_pass;
        private MetroFramework.Controls.MetroLabel lbl_recup_contrasena;
        private MetroFramework.Controls.MetroPanel metroPanel1;
    }
}


﻿namespace VISTA
{
    partial class pnl_recupContrasena
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(pnl_recupContrasena));
            this.txt_emailRecuperar = new MetroFramework.Controls.MetroTextBox();
            this.btn_borrar_txt_recup = new MetroFramework.Controls.MetroButton();
            this.btn_enviarContrasena = new MetroFramework.Controls.MetroButton();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lbl_email = new MetroFramework.Controls.MetroLabel();
            this.lbl_recuperar_contrasena = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt_emailRecuperar
            // 
            this.txt_emailRecuperar.BackColor = System.Drawing.Color.LightGray;
            // 
            // 
            // 
            this.txt_emailRecuperar.CustomButton.Image = null;
            this.txt_emailRecuperar.CustomButton.Location = new System.Drawing.Point(260, 1);
            this.txt_emailRecuperar.CustomButton.Name = "";
            this.txt_emailRecuperar.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txt_emailRecuperar.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_emailRecuperar.CustomButton.TabIndex = 1;
            this.txt_emailRecuperar.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_emailRecuperar.CustomButton.UseSelectable = true;
            this.txt_emailRecuperar.CustomButton.Visible = false;
            this.txt_emailRecuperar.Lines = new string[0];
            this.txt_emailRecuperar.Location = new System.Drawing.Point(143, 97);
            this.txt_emailRecuperar.MaxLength = 32767;
            this.txt_emailRecuperar.Name = "txt_emailRecuperar";
            this.txt_emailRecuperar.PasswordChar = '\0';
            this.txt_emailRecuperar.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_emailRecuperar.SelectedText = "";
            this.txt_emailRecuperar.SelectionLength = 0;
            this.txt_emailRecuperar.SelectionStart = 0;
            this.txt_emailRecuperar.ShortcutsEnabled = true;
            this.txt_emailRecuperar.Size = new System.Drawing.Size(282, 23);
            this.txt_emailRecuperar.TabIndex = 0;
            this.txt_emailRecuperar.UseCustomBackColor = true;
            this.txt_emailRecuperar.UseCustomForeColor = true;
            this.txt_emailRecuperar.UseSelectable = true;
            this.txt_emailRecuperar.UseStyleColors = true;
            this.txt_emailRecuperar.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_emailRecuperar.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btn_borrar_txt_recup
            // 
            this.btn_borrar_txt_recup.BackColor = System.Drawing.Color.Transparent;
            this.btn_borrar_txt_recup.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_borrar_txt_recup.Location = new System.Drawing.Point(422, 97);
            this.btn_borrar_txt_recup.Name = "btn_borrar_txt_recup";
            this.btn_borrar_txt_recup.Size = new System.Drawing.Size(26, 23);
            this.btn_borrar_txt_recup.TabIndex = 2;
            this.btn_borrar_txt_recup.Text = "X";
            this.btn_borrar_txt_recup.UseCustomBackColor = true;
            this.btn_borrar_txt_recup.UseCustomForeColor = true;
            this.btn_borrar_txt_recup.UseSelectable = true;
            this.btn_borrar_txt_recup.UseStyleColors = true;
            this.btn_borrar_txt_recup.Click += new System.EventHandler(this.btn_borrar_txt_recup_Click);
            // 
            // btn_enviarContrasena
            // 
            this.btn_enviarContrasena.BackColor = System.Drawing.Color.MidnightBlue;
            this.btn_enviarContrasena.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_enviarContrasena.ForeColor = System.Drawing.Color.White;
            this.btn_enviarContrasena.Location = new System.Drawing.Point(86, 168);
            this.btn_enviarContrasena.Name = "btn_enviarContrasena";
            this.btn_enviarContrasena.Size = new System.Drawing.Size(369, 48);
            this.btn_enviarContrasena.TabIndex = 4;
            this.btn_enviarContrasena.Text = "ENVIAR";
            this.btn_enviarContrasena.Theme = MetroFramework.MetroThemeStyle.Light;
            this.btn_enviarContrasena.UseCustomBackColor = true;
            this.btn_enviarContrasena.UseCustomForeColor = true;
            this.btn_enviarContrasena.UseSelectable = true;
            this.btn_enviarContrasena.UseStyleColors = true;
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroPanel1.BackgroundImage")));
            this.metroPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.metroPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel1.Controls.Add(this.lbl_recuperar_contrasena);
            this.metroPanel1.Controls.Add(this.lbl_email);
            this.metroPanel1.Controls.Add(this.btn_enviarContrasena);
            this.metroPanel1.Controls.Add(this.btn_borrar_txt_recup);
            this.metroPanel1.Controls.Add(this.txt_emailRecuperar);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(8, 89);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(553, 283);
            this.metroPanel1.TabIndex = 5;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.BackColor = System.Drawing.Color.Transparent;
            this.lbl_email.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lbl_email.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_email.Location = new System.Drawing.Point(86, 97);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(51, 19);
            this.lbl_email.Style = MetroFramework.MetroColorStyle.Blue;
            this.lbl_email.TabIndex = 5;
            this.lbl_email.Text = "E-Mail";
            this.lbl_email.UseCustomBackColor = true;
            this.lbl_email.UseCustomForeColor = true;
            this.lbl_email.UseStyleColors = true;
            // 
            // lbl_recuperar_contrasena
            // 
            this.lbl_recuperar_contrasena.AutoSize = true;
            this.lbl_recuperar_contrasena.BackColor = System.Drawing.Color.Transparent;
            this.lbl_recuperar_contrasena.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lbl_recuperar_contrasena.ForeColor = System.Drawing.Color.White;
            this.lbl_recuperar_contrasena.Location = new System.Drawing.Point(143, 52);
            this.lbl_recuperar_contrasena.Name = "lbl_recuperar_contrasena";
            this.lbl_recuperar_contrasena.Size = new System.Drawing.Size(282, 19);
            this.lbl_recuperar_contrasena.TabIndex = 6;
            this.lbl_recuperar_contrasena.Text = "Ingrese su correo electrónico registrado.";
            this.lbl_recuperar_contrasena.UseCustomBackColor = true;
            this.lbl_recuperar_contrasena.UseCustomForeColor = true;
            this.lbl_recuperar_contrasena.UseStyleColors = true;
            // 
            // pnl_recupContrasena
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackImage = ((System.Drawing.Image)(resources.GetObject("$this.BackImage")));
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(566, 450);
            this.Controls.Add(this.metroPanel1);
            this.Name = "pnl_recupContrasena";
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.None;
            this.Text = "Recuperar Contraseña";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TransparencyKey = System.Drawing.Color.Black;
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox txt_emailRecuperar;
        private MetroFramework.Controls.MetroButton btn_borrar_txt_recup;
        private MetroFramework.Controls.MetroButton btn_enviarContrasena;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel lbl_email;
        private MetroFramework.Controls.MetroLabel lbl_recuperar_contrasena;
    }
}